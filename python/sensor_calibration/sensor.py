"""Data handlers for Mysterious Sensors.
"""
import os

from functools import lru_cache

import numpy as np
from sklearn import linear_model
from scipy import signal, interpolate
from scipy.signal import butter, filtfilt
from statsmodels.nonparametric.smoothers_lowess import lowess


from sensor_calibration import base, proc, node


class Graph(object):
    """Hold processing node graph. Run graph through dependencies.
    """

    def __init__(self, out_prefix:str, ref_csv: str, *csv_paths: str):
        """
        Args:
            out_prefix (str): output file prefix
            ref_csv (str): A csv file path for the reference data
            *csv_paths (str): All other csv files to calibrate
        """
        self.csv_ref = ref_csv
        self.csv_sensors = csv_paths
        self.prefix = out_prefix

        # add read nodes (initiators)
        self.ref_read = node.ReadNode(ref_csv)
        self.ref_read.name = 'Read 0'
        self.sensor_reads = [node.ReadNode(path) for path in csv_paths]
        for i, nd in enumerate(self.sensor_reads, start=1):
            nd.name = f'Read {i}'

        # add denoise filter
        self.ref = node.RemoveNoiseNode(self.ref_read)
        self.ref.name = 'Denoise 0'
        self.ref_smooth = node.SmoothNode(self.ref)
        self.sensors = [node.RemoveNoiseNode(nd) for nd in self.sensor_reads]
        for i, nd in enumerate(self.sensors, start=1):
            nd.name = f'Denoise {i}'

        # add plot - original
        img_path_orig = self._get_out_path('original.png')
        self.image_original = node.PlotNode(
            img_path_orig, self.ref, *self.sensors)
        self.image_original.title = 'Plot 1: Original Mesaurement'
        self.image_original.name = 'Plot 1'

        # add regression
        self.predicts = [
            node.RegressionNode(self.ref, nd) for nd in self.sensors
        ]
        for i, nd in enumerate(self.predicts, start=1):
            nd.name = f'Regression {i}'

        # add plot - corrected
        img_path_corr = self._get_out_path('corrected.png')
        self.image_corrected = node.PlotNode(
            img_path_corr, self.ref, *self.predicts)
        self.image_corrected.title = 'Plot 2: Corrected Mesurement'
        self.image_corrected.name = 'Plot 2'

        # add residuals
        self.residuals = [node.ResidualNode(self.ref, self.ref)]
        self.residuals += [
            node.ResidualNode(self.ref, nd) for nd in self.predicts
        ]
        for i, nd in enumerate(self.residuals, start=0):
            nd.name = f'Residual {i}'

        # add plot - residual
        img_path_resi = self._get_out_path('residual.png')
        self.image_residual = node.PlotNode(
            img_path_resi, *self.residuals)
        self.image_residual.title = 'Plot 3: Residual'
        self.image_residual.ylim = (-1, 1)
        self.image_residual.name = 'Plot 3'

        # collect ternimator nodes
        self.terminators = [
            self.image_original,
            self.image_corrected,
            self.image_residual,
        ]

    def _get_out_path(self, suffix:str):
        return f'{self.prefix}.{suffix}'

    def run(self):
        for terminator in self.terminators:
            yield terminator()

        self.report()

    def report(self):
        """Write a report file with processing result.
        """
        report_filtered = []
        ref_raw, ref_filtered = self.ref_read(), self.ref()
        report_filtered.append([len(ref_filtered),len(ref_raw)])
        for raw, filtered in zip(self.sensor_reads, self.sensors):
            report_filtered.append([len(filtered()),len(raw())])

        report_residual = []
        residuals = [r() for r in self.residuals]
        for residual in self.residuals:
            values = residual()
            total = sum(map(abs, values[:, 1])) / len(values)
            report_residual.append(total)

        with open(f'{self.prefix}.report.md', 'w') as fw:
            fw.write('# Filter Result #\n\n')
            fw.write('| Sensor | Filtered Out | Total |\n')
            fw.write('| ------ | ------------ | ----- |\n')
            for i, filtered in enumerate(report_filtered):
                survived, total = filtered
                dropped = total - survived
                txt = f'| {i:6d} | {dropped:12d} | {total:5d} |\n'
                fw.write(txt)
            fw.write('\n\n')

            fw.write('# Residual #\n\n')
            fw.write('| Sensor |   Residual   |\n')
            fw.write('| ------ | ------------ |\n')
            for i, residual in enumerate(report_residual):
                txt = f'| {i:6d} | {residual:12g} |\n'
                fw.write(txt)
            fw.write('\n\n')
