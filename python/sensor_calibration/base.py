"""
"""
from functools import lru_cache

from sensor_calibration import get_logger


logger = get_logger()


class Node(object):

    name = 'Original'

    def __init__(self, upstream=None):
        logger.info('Initiating %s: %s', self.name, upstream)
        self.upstream = upstream

    @lru_cache
    def __call__(self):

        if self.upstream:
            input = self.upstream()
        else:
            input = None

        logger.info('Running %s', self.name)
        logger.debug('Data for %s: %s', self.name, input)

        return self.execute(input)

    def execute(self, array):
        # do nothing
        return array


class Merge(Node):

    name = 'Merge'

    def __init__(self, *upstreams:Node):
        logger.info('Initiating %s: %s', self.name, upstreams)
        self.upstreams = upstreams

    @lru_cache
    def __call__(self):

        if self.upstreams:
            inputs = [nd() for nd in self.upstreams]
        else:
            inputs = []

        logger.info('Running %s', self.name)
        logger.debug('Data for %s: %s', self.name, input)

        return self.execute(*inputs)

    def execute(self, *arrays):
        # do nothing
        return arrays
