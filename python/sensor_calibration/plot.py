"""
"""
from matplotlib import pyplot


def create_plot(title, xlabel='time', ylabel='measurement'):
    """Create a new plot container.
    Arguments:
        title (str): A Plot title
        xlabel (str): A text for the x axis Default to "time".
        ylabel (str): A text for the y axis. Default to "measurement".

    Returns:
        (matplotlib.Figure, matplotlib.Axes) New figure and axes
    """
    fig, ax = pyplot.subplots()

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_ylim(-1, 2)
    ax.set_xlim(0, 1)
    ax.set_title(title)

    ax.grid(True)
    fig.tight_layout()

    return fig, ax


def add_scatter(
    x_data, y_data, axes, size=1.0, color=None, alpha=0.5, label=None):
    axes.scatter(x_data, y_data, s=size, alpha=alpha, label=label)


def save(fig, image_path, dpi=300):
    fig.savefig(image_path, dpi=dpi)
    pyplot.close()
