"""Data handlers for Mysterious Sensors.
"""
import os

import numpy as np
from sensor_calibration import base, proc, plot


class RemoveNoiseNode(base.Node):

    name = 'RemoveNoise'

    def execute(self, array):
        return proc.noise_filter(array)


class SmoothNode(base.Node):

    name = 'Smooth'

    def execute(self, array):
        return proc.loess_filter(array, frac=0.1)


class RegressionNode(base.Node):

    name = 'Regression'

    def __init__(self, reference:base.Node, upstream:base.Node=None):
        self.reference = reference
        super(RegressionNode, self).__init__(upstream)

    def execute(self, array):
        ref = self.reference()
        regr = proc.get_linear_regression(array, ref)
        return regr.predict(array)


class ResidualNode(base.Node):

    name = 'Residual'

    def __init__(self, reference:base.Node, upstream:base.Node=None):
        self.reference = reference
        super(ResidualNode, self).__init__(upstream)

    def execute(self, array):
        ref = self.reference()
        residuals = proc.get_residual(array, ref)
        output = np.zeros([len(residuals), 2])
        output[:, 0], output[:, 1] = array[:, 0], residuals
        return output


class ReadNode(base.Node):

    name = 'Read'

    def __init__(self, filename:str):
        super(ReadNode, self).__init__(None)

        self.filename = filename

    def execute(self, array=None):
        return proc.read_csv(self.filename)


class PlotNode(base.Merge):

    name = 'Plot'

    def __init__(self, image_path:str, *upstreams:base.Node):
        super(PlotNode, self).__init__(*upstreams)

        self.filepath = image_path
        self.title = 'Plot'
        self.xlabel = 'time'
        self.ylabel = 'measurement'
        self.ylim = (-1, 2)

    def execute(self, *arrays):
        fig, axes = plot.create_plot(self.title, self.xlabel, self.ylabel)

        axes.set_ylim(*self.ylim)

        for arr in arrays:
            plot.add_scatter(arr[:, 0], arr[:, 1], axes)

        plot.save(fig, self.filepath)

        return []
