import logging


LOG_FORMAT = '[%(asctime)s][%(name)s] %(levelname)s: %(message)s'

_LOGGER = None


def get_logger():
    global _LOGGER
    if _LOGGER is None:
        logger = logging.getLogger('sensor_calibration')
        logger.setLevel(logging.INFO)

        stream_handler = logging.StreamHandler()
        stream_formatter = logging.Formatter(LOG_FORMAT)
        stream_handler.setFormatter(stream_formatter)
        logger.addHandler(stream_handler)
        _LOGGER = logger

    return _LOGGER
