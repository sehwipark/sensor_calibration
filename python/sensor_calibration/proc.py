"""Data handlers for Mysterious Sensors.
"""
import os
import csv

import numpy as np
from sklearn import linear_model
from scipy import signal, interpolate
from scipy.signal import butter, filtfilt
from statsmodels.nonparametric.smoothers_lowess import lowess


def get_linear_regression(train_data: np.array, reference: np.array):
    """Get a Linear Regression fit to this data.

    Arguments:
        array (numpy.array): An array to train the model.

    Returns:
        (sklearn.LinearRegression) A model fitted to this data.
    """
    t1 = train_data[:, 0]
    func_interp = _interpolate(reference)
    resampled = np.array([(t, func_interp(t)) for t in t1])

    regr = linear_model.LinearRegression()
    regr.fit(train_data, resampled)

    return regr


def _interpolate(array):
    f = interpolate.interp1d(
        array[:, 0], array[:, 1],
        kind='linear', bounds_error=False, fill_value='extrapolate')
    return f


def get_residual(array: np.array, reference):
    """Get residuals

    Arguments:
        array (numpy.array): 2D numpy matrix

    Returns:
        (list) Residual values of all sample points.
    """
    t1 = array[:, 0]
    func_interp = _interpolate(reference)
    resampled = [func_interp(t) for i, t in enumerate(t1)]
    return [x - r for x, r in zip(array[:, 1], resampled)]


def loess_filter(array, frac=0.05):
    """Smooth data with Loess filter.

    Args:
        array (numpy.array): A 2d Array
        frac (float): A fraction value
    Returns:
        (numpy.array) A filtered array
    """
    filtered = lowess(array[:, 1], array[:, 0], frac=frac)

    return filtered


def noise_filter(array, kernel_size=15, noise_to_signal=5):
    """A simple low pass filter to remove pulse noise.
        t = [0..1]
    """
    t1 = array[:, 0]
    dt1 = np.diff(t1)
    max_dt = max(dt1)

    y1 = array[:, 1]
    n = len(y1)

    # using that sampling freq. is much higher than the signal freq.
    # y2 = signal.medfilt(y1, kernel_size=[kernel_size])
    d2 = loess_filter(array, frac=0.05)
    y2 = d2[:, 1]

    # find a reasonable data range, and trim data.
    #   assuming noise is not more than 5%
    y_sorted = sorted(y1)
    bottom, top = y_sorted[int(n * 0.03)], y_sorted[int(n * 0.97)]
    amp = top - bottom

    values = []
    for i in range(len(t1)):
        t, y = t1[i], y1[i]
        threashold = amp  * noise_to_signal * max_dt
        if abs(y - y2[i]) > threashold:
            continue
        values += [(t, y)]

    return np.array(values)


def lowpass_filter(vector, cutoff=1e2, freq=1e3, order=4):
    """Butterworth low pass filter.
    """
    nyq = 0.5 * freq
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, vector)
    return y


def read_csv(csv_path: str):
    """Read a csv file and create this.

    Arguments:
        csv_path (str): A csv file path

    Returns:
        (np.array) 2d float array
    """
    def _iter_csv(filename):
        with open(filename, 'r') as f:
            for row in csv.reader(f, delimiter=','):
                yield list(map(float, row))

    rows = list(_iter_csv(csv_path))

    return np.array(rows)
