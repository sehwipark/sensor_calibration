#! /usr/bin/env python3
import argparse

from sensor_calibration import sensor


def main(args):
    ref = args.reference
    sources = args.csv_paths

    num_sources = len(sources)
    prefix = args.out_prefix or str(args.reference)

    graph = sensor.Graph(prefix, ref, *sources)

    for i, stage in enumerate(graph.run()):
        print(f'Done Stage {i}')



if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Render image from a csv.'
    )
    parser.add_argument(
        'csv_paths', type=str, nargs='*',
        help='csv file paths',
    )
    parser.add_argument(
        '--reference', '-r', type=str,
        help='A reference data csv file.',
        required=True,
    )
    parser.add_argument(
        '--out-prefix', '-o', type=str,
        help='Output file prefix.',
    )
    args = parser.parse_args()

    main(args)
