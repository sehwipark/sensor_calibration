test:
	pip install -q ".[test]"
	coverage run -m pytest -v --junitxml results.xml tests/
	@ #coverage html;coverage report

install:
	pip install -q ".[test]"

clean:
	pip uninstall -y sensor_calibration

run:
	calibrate_sensor.py -o doc/test --reference tests/data/sensor_*.csv
