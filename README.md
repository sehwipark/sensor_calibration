# Sensor Calibration #


## Install ##

```
$ pip install .
```


## Usage ##

```
$ calibrate_sensor.py --out-prefix result --reference sensor_0.csv sensor_1.csv ...

# Files will be created
    result.report.md
    result.original.png
    result.corrected.png
    result.residual.png
```


## Example ##

### example.report.md  ###

```
# Filter Result #
| Sensor | Filtered Out | Total |
| ------ | ------------ | ----- |
|      0 |          193 |  1000 |
|      1 |          206 |  1000 |
|      2 |          303 |  1000 |
...

# Residual #
| Sensor |  Residual  |
| ------ | ---------- |
|      0 |          0 |
|      1 |  0.0747124 |
|      2 |   0.114502 |
...
```

### result.original.png ###

![Original Measurement](doc/result.original.png "Original")


### result.corrected.png ###

![Corrected Measurement](doc/result.corrected.png "Corrected")

### result.residual.png ###


![Prediction Residual](doc/result.residual.png "Residual")
