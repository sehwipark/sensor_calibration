import pytest


@pytest.fixture
def data_dir():
    import os
    cur_dir = os.path.dirname(__file__)
    return os.path.join(cur_dir, 'data')


@pytest.fixture
def reference_file(data_dir):
    import os
    return os.path.join(data_dir, 'sensor_0.csv')


@pytest.fixture
def table(reference_file):
    import os
    import csv
    with open(reference_file, 'r') as f:
        rows = csv.reader(f, delimiter=',')
        yield rows
    return


@pytest.fixture
def data(reference_file):
    import os
    from sensor_calibration.sensor import SensorData

    ref = SensorData.from_csv(reference_file)
    return ref


def test_load_csv(table):
    for row in table:
        assert row
    return
