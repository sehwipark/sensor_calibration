import pytest


@pytest.fixture
def array_uniform():
    import math
    import numpy

    n = 100
    array = numpy.zeros([n, 2])
    array[:, 0] = [x / float(n - 1) for x in range(n)]
    array[:, 1] = [
        math.sin(2 * math.pi * t * 10) + 0.2 * math.cos(2 * math.pi * t * 20)
        for t in array[:, 0]
    ]

    return array


@pytest.fixture
def array_random(array_uniform):
    import math
    import numpy

    n = 100
    array = numpy.zeros([n, 2])
    cnt = 0
    array[:, 0] = [
        array_uniform[i, 0] + array_uniform[i, 1] / (3 * n) for i in range(n)]
    array[:, 1] = [
        math.sin(2 * math.pi * t * 10) + 0.2 * math.cos(2 * math.pi * t * 20)
        for t in array[:, 0]
    ]

    return array


def test_make_array(array_uniform):
    print(array_uniform)
    assert True


def test_make_array_2(array_random):
    print(array_random)
    assert True

